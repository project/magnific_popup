api = 2
core = 7.x

libraries[magnific_popup][download][type] = "get"
libraries[magnific_popup][download][url] = "https://github.com/dimsemenov/Magnific-Popup/archive/master.zip"
libraries[magnific_popup][download][subtree] = "Magnific-Popup-master"
libraries[magnific_popup][directory_name] = "magnific-popup"
libraries[magnific_popup][destination] = "libraries"
